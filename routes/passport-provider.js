
var express = require('express');
var router = express.Router();
var viewPageCount = 0;

const mongo = require('mongodb'),
    mongoose = require('mongoose');
mongoose.connect("mongodb://localhost:27017/chat");

let db = mongoose.connection;
db.on('error', function () {
    console.log('connection error');
});

db.once('open', function () {
    console.log("Connection établie");
});

const Schema = mongoose.Schema;
var userSchema = new Schema({
    name: String,
    surname: String
});


var passport = require('passport')
    , FacebookStrategy = require('passport-facebook').Strategy;

passport.use(new FacebookStrategy({
        clientID: FACEBOOK_APP_ID,
        clientSecret: FACEBOOK_APP_SECRET,
        callbackURL: "http://www.example.com/auth/facebook/callback"
    },
    function(token, tokenSecret, profile, done) {
        var User = mongoose.model('User', userSchema);

        var myUser = new User({
            name:profile.name.givenName,
            surname: profile.name.familyName
        });

        myUser.save();
        console.log(myUser);

        User.findOrCreate(..., function(err, user) {
            if (err) { return done(err); }
            done(null, user);
        });
    }
));

// Redirect the user to Facebook for authentication.  When complete,
// Facebook will redirect the user back to the application at
//     /auth/facebook/callback

app.get('/auth/facebook',
    passport.authenticate('facebook', { scope: 'read_stream' })
);
// Facebook will redirect the user to this URL after approval.  Finish the
// authentication process by attempting to obtain an access token.  If
// access was granted, the user will be logged in.  Otherwise,
// authentication has failed.
app.get('/auth/facebook/callback',
    passport.authenticate('facebook', { successRedirect: '/',
        failureRedirect: '/login' }));