var express = require('express');
var router = express.Router();
var viewPageCount = 0;
const request = require("request");
const url =
    "https://reqres.in/api/users";

const mongo = require('mongodb'),
    mongoose = require('mongoose');
mongoose.connect("mongodb://localhost:27017/testdb");

let db = mongoose.connection;
db.on('error', function () {
    console.log('connection error');
});

db.once('open', function () {
    console.log("Connection établie");
});

const Schema = mongoose.Schema;
var userSchema = new Schema({
    name: String,
    surname: String
});

/*********** Routes ****************/


router.all('*', function (req,res,next) {
    viewPageCount = viewPageCount +1;
    next();
});

/* GET users listing. */
router.get('/', function(req, res, next) {

    mongoose.model('User', userSchema).find({},function (error,items) {
        res.render('user/user_list',{datas: items, views: viewPageCount});
    });

});

/*create*/

router.post('/', function(req, res, next) {

    var User = mongoose.model('User', userSchema);

    var myUser = new User({
        name:req.body.name,
        surname: req.body.surname
    });

    myUser.save();
    console.log(myUser);

    res.redirect('/users');
});

module.exports = router;

