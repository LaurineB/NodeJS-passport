// Require needed dependencies
const express = require('express');
const app = express();
const server = app.listen(3000);
const io = require('socket.io').listen(server);

const mongo = require('mongo'),
    mongoose = require('mongoose');

// Mongo startup and configuration
const Schema = mongoose.Schema;
mongoose.connect('mongodb://127.0.0.1:27017/chat');

let db = mongoose.connection;
let messageSchema = new Schema({
    message: String,
    sender: String,
    date: Date
});
let Message = mongoose.model('Message', messageSchema);

db.on('error', function() {
    console.log("Error connecting database");
    process.exit(-1);
});

db.on('connect', function() {
    // Do something here if you want to
});

// "Routing"
app.use(express.static(__dirname + "/public"));

// Socket IO default username
const defaultName = 'Anonymous_';
io.on('connection', function(socket) {

    // Get last messages
    var offset = new Date();
    offset.setMinutes(offset.getMinutes() - 15);

    var messages = Message.find({
        date: { $gt: offset }
    }, function(err, rows) {
        socket.emit('affect', {
            messages: rows,
            username: defaultName + socket.id
        });
    });

    socket.on('message', function(data) {
        var $data = JSON.parse(data);
        $data.date = new Date();
        Message.create($data, function(err, message) {
            if (!err) io.emit('news', $data);
        });
    });
});


/* Passport provider */

var userSchema = new Schema({
    name: String,
    surname: String
});

var passport = require('passport')
    , FacebookStrategy = require('passport-facebook').Strategy;

var User = mongoose.model('User', userSchema);
var sessionObj = session({ secret : 'ThisIsASecretToken'});
app.use(sessionObj);
app.use(passport.initialize());
app.use(passport.session());
passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
        done(err, user);
    });
});

passport.use(new FacebookStrategy({
        clientID: "154837355224029",
        clientSecret: "080a89052afc7a4f558f93f4f98156f4",
        callbackURL: "http://localhost:3000/auth/facebook/callback/"
    },
    function(token, tokenSecret, profile, done) {


        var myUser = new User({
            name:profile.name.givenName,
            surname: profile.name.familyName
        });

        myUser.save();
        console.log(myUser);

        /*
         User.findOrCreate("Laurine", function(err, user) {
            if (err) { return done(err); }

        });
         */
        done(null, profile);
    }
));

// Redirect the user to Facebook for authentication.  When complete,
// Facebook will redirect the user back to the application at
//     /auth/facebook/callback

app.get('/auth/facebook',
    passport.authenticate('facebook')
);
// Facebook will redirect the user to this URL after approval.  Finish the
// authentication process by attempting to obtain an access token.  If
// access was granted, the user will be logged in.  Otherwise,
// authentication has failed.
app.get('/auth/facebook/callback',
    passport.authenticate('facebook', { successRedirect: '/',
        failureRedirect: '/login' }));